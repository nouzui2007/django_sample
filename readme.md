# DockerでDjango環境を作る

## Djangoベースイメージ作成

Dockerファイルからイメージを作成する。

```
$ docker build -t django . 
```

## 起動

djangoがインストールされたイメージからコンテナを作成して起動する。

このとき、ホスト側のsrcフォルダを/opt/appsディレクトリにマウントする

```
$ docker run -itd -p 8000:8000 -v /c/Users/hotaka-makiuchi/Documents/docker/django/src:/opt/apps --name django django
```

## Djangoプロジェクトの作成

プロジェクトを作成する。
作成先は/opt/app

以下のコマンドの最後の「.」は、プロジェクトを作成する場所を示す。
これがないと、/opt/apps/djangosample/djangosampleと入れ子が深くなる。

```
$ docker exec django-container django-admin startproject djangosample .
```

ディレクトリ構成は以下の通り。

```
│  manage.py
│
└─djangosample
        settings.py
        urls.py
        wsgi.py
        __init__.py
```

## Djangoアプリの作成

todoアプリを作成する。

```
$ docker exec django python manage.py startapp todo
```

## Migrate

manage.py migrate

## 管理ユーザ

manage.py createsuperuser

## 開発用サーバを起動

```
$ docker exec django python manage.py runserver 0.0.0.0:8000
```

## DRF

### インストール

```
pip install djangorestframework
pip install django-filter
pip install djangorestframework-jwt

```

### 設定

プロジェクトの`setting.py`の**INSTALLED_APPS**に、**rest_framework**、**django_filters**を追加する。

```
INSTALLED_APPS = [
    'django_filters',
    'rest_framework',
    'todo',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

```
REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',) 
}
```

## 認証

### インストール

```
```

### 設定


### 確認

curlで確認する。

WindowsのcurlはWeb-Requestのエイリアスなので、curl.exeは実行されない。
そのため、エイリアスを削除する必要がある。

```
get-alias curl

del alias:curl
```

トークンを取得して、APIを呼び出す。

```
curl -X POST -d "username=admin&password=admin" http://localhost:8000/api-token-auth/
curl -X POST -H "Content-Type: application/json" -d '{"username":"admin","password":"admin"}' http://localhost:8000/api-token-auth/

 curl -H "Authorization: JWT <your_token>" http://localhost:8000/protected-url/
```


## TIPS

### マイグレーション

プロジェクトの`setting.py`の**INSTALLED_APPS**に、アプリケーション名**todo**を追加する。

```
INSTALLED_APPS = [
    'todo',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

マイグレーション用のファイルを作成して、マイグレーションを実施する。

```
$ docker exec django ./manage.py makemigrations todo

$ docker exec django ./manage.py migrate
```

### カスタムユーザ

AbstractUser / AbstractBaseUser を継承した、ユーザ管理用のモデルを作成する。

例：todo.Account

setting.pyに以下の設定を追加する。

```
AUTH_USER_MODEL = 'api.todo.Account'
```

setting.pyのINSTALLED_APPSから、adminをコメントアウトする。

これはユーザ関連のテーブル情報がadminからtodoに移行するので、これをやらないと依存関係でエラーになる。


```
INSTALLED_APPS = [
    'todo',
    # 'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

マイグレーションを行う。

```
./manage.py makemigrations

./manage.py migrate
```

### 管理サイト

管理サイトのユーザを作成する。

ここでは、インタラクティブに作成するコマンドを使うため、コンテナ内に入って作業する。

```
./manage.py createsuperuser
```

管理サイトにモデルを追加する。

`admin.py`ファイルを**todo**に追加する。

```
from django.contrib import admin
from todo.models import *

admin.site.register(Users)
```

## Debug tools
pip install django-debug-toolbar

settings.pyに追記

```
if DEBUG:
    INSTALLED_APPS = [
        'debug_toolbar',
    ] + INSTALLED_APPS

    MIDDLEWARE = [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ] + MIDDLEWARE

    INTERNAL_IPS = type(str('c'), (), {'__contains__': lambda *a: True})()
```

プロジェクトのurls.pyに追記

```
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
```