from django.urls import path

from . import views

urlpatterns = [
    path('<int:id>', views.index, name='index'),
    path('t/<str:param>', views.template, name='template'),
    path('s/<str:param>', views.shortcut, name='shortcut'),
]
