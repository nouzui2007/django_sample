from django.http.response import HttpResponse
from django.shortcuts import render
from django.template import loader

def index(request, id):
    return HttpResponse('This is urls test. id = ' + str(id))

def template(request, param):
    template = loader.get_template('sampleapp/index.html')
    context = {
        'var_from_view': 'This is parameter from view ' + param,
    }
    return HttpResponse(template.render(context, request))

def shortcut(request, param):
    context = {
        'var_from_view': 'This is parameter from view ' + param,
    }
    return render(request, 'sampleapp/shortcut.html', context)